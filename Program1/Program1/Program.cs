﻿/*
 * Created by SharpDevelop.
 * User: Dunkstormen
 * Date: 10-05-2017
 * Time: 09:46
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Program1
{
	class Program
	{
		public static void Main(string[] args)
		{
			// Definere variabler
			int tal;
			
			// Sætter værdien på et variabel

			while ( true ) {
				
				// Sætter værdien på et variabel
				Console.Write("Indtast et tal -> ");
				tal = int.Parse(Console.ReadLine());
			
				if( tal == 10 ) {
					Console.WriteLine("Du gættede tallet!");
					break;
				} else if ( tal >= 10 ) {
					Console.WriteLine("Tallet er mindre!");
				} else {
					Console.WriteLine("Tallet er størrer!");
				}
			}
			
			Console.ReadKey();
		}
	}
}